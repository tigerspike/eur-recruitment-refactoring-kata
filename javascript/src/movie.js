const childrens = 2,
  regular = 0,
  newRelease = 1;

class Movie {

  constructor(title, priceCode) {
    this.title = title;
    this.priceCode = priceCode;
  }

  getPriceCode() {
    return this.priceCode;
  }

  setPriceCode(priceCode) {
    this.priceCode = priceCode;
  }

  getTitle() {
    return this.title;
  }

  static CHILDRENS() {
    return childrens;
  }

  static REGULAR() {
    return regular;
  }

  static NEW_RELEASE() {
    return newRelease;
  }
}

export default Movie;